require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'text_field/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'text_field'
  s.version = TextField::VERSION
  s.authors = ['Tobias Grasse']
  s.email = ['tg@glancr.de']
  s.homepage = 'https://glancr.de/widgets/text_field'
  s.summary = 'mirr.OS Widget to display custom text.'
  s.description = 'Show custom text (and HTML) on your glancr.'
  s.license = 'MIT'
  # noinspection SpellCheckingInspection
  s.metadata = { 'json' =>
                   {
                     type: 'widgets',
                     title: {
                       enGb: 'Text',
                       deDe: 'Text',
                       frFr: 'Texte',
                       esEs: 'Texto',
                       plPl: 'Tekst',
                       koKr: '본문'
                     },
                     description: {
                       enGb: s.description,
                       deDe: 'Zeige individuellen Text (und HTML) auf deinem glancr an.',
                       frFr: 'Affiche le texte individuel (et HTML) sur votre glancr.',
                       esEs: 'Mostrar texto individual (y HTML) en la pantalla.',
                       plPl: 'Wyświetlanie pojedynczego tekstu (i HTML) na glancr.',
                       koKr: 'glancr에 개별 텍스트 (및 HTML)를 표시하십시오.'
                     },
                     sizes: [
                       { w: 4, h: 4 },
                       { w: 4, h: 2 },
                       { w: 6, h: 4 },
                       { w: 12, h: 4 },
                       { w: 12, h: 8 },
                       { w: 12, h: 12 },
                       { w: 12, h: 21 },
                       { w: 21, h: 6 },
                       { w: 21, h: 12 }
                     ],
                     languages: %i[enGb deDe frFr esEs plPl koKr],
                     group: nil
                   }.to_json }
  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
end
