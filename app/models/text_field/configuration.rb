# frozen_string_literal: true

module TextField
  class Configuration < WidgetInstanceConfiguration
    attribute :content, :string, default: ""
  end
end
